package com.htc.ea.departmentservice.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class DepartmentRequest {
	
	@NotNull
	private Long idOrganization=1L;
	@NotNull
	@Length(min=1,max=40)
	private String name="";
	
	public DepartmentRequest() {
		super();
	}
	
	public DepartmentRequest(Long idOrganization, String name) {
		super();
		this.idOrganization = idOrganization;
		this.name = name;
	}
	public Long getIdOrganization() {
		return idOrganization;
	}
	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DepartmentRequest [");
		if (idOrganization != null) {
			builder.append("idOrganization=");
			builder.append(idOrganization);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
}
