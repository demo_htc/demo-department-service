package com.htc.ea.departmentservice.client;

import org.springframework.http.ResponseEntity;

public interface OrganizationApiClient {

	/**
	 * consulta a microservicio organization
	 * retorna un valor boolean en el body donde: es true si se encontro, false si no
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y valor boolean en el body
	 */
	public ResponseEntity<Boolean> organizationExist(Long id);
}
