package com.htc.ea.departmentservice.client.impl;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.departmentservice.client.EmployeeApiClient;
import com.htc.ea.departmentservice.config.RestTemplateConfig;
import com.htc.ea.departmentservice.model.Employee;

@Component
public class EmployeeApiClientImpl implements EmployeeApiClient {

	
	@Value("${service.employee}")
    private String baseUri;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	RestTemplateConfig restConfig;
	
	/**
	 * direccion base donde se encuentra alojado el servicio de empleado
	 */
	@Override
	public ResponseEntity<List<Employee>> findEmployeesByIdDepartment(Long id) {
		HttpHeaders headers = new HttpHeaders();
		try {
			StringBuilder composedUri = new StringBuilder();
			composedUri.append(baseUri); 			//https:{ip}:{puerto}
			composedUri.append("/departments/"); 	//uri resultante 
			composedUri.append(String.valueOf(id));	//parametro
			
			
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<List<Employee>> entity = new HttpEntity<>(headers);
					
			return restConfig.getRestTemplate().exchange(composedUri.toString(), HttpMethod.GET,entity, new ParameterizedTypeReference<List<Employee>>(){});
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
			headers.add("Error", "No se pudo acceder al servicio");
			return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
		}
	}

}
