package com.htc.ea.departmentservice.client.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.departmentservice.client.OrganizationApiClient;
import com.htc.ea.departmentservice.config.RestTemplateConfig;
import com.htc.ea.departmentservice.model.Employee;

@Component
public class OrganizationApiClientImpl implements OrganizationApiClient{

	@Value("${service.organization}")
    private String baseUri;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	RestTemplateConfig restConfig;
	
	@Override
	public ResponseEntity<Boolean> organizationExist(Long id) {
		HttpHeaders headers = new HttpHeaders();
		try {
			StringBuilder composedUri = new StringBuilder();
			composedUri.append(baseUri); 			//http:{ip}:{puerto}
			composedUri.append("/");
			composedUri.append(String.valueOf(id));
			composedUri.append("/exist"); 	//uri resultante 
			
			HttpEntity<List<Employee>> entity = new HttpEntity<>(headers);
					
			return restConfig.getRestTemplate().exchange(composedUri.toString(), HttpMethod.GET,entity, Boolean.class);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
			headers.add("Error", "No se pudo acceder al servicio");
			return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
		}
	}

}
