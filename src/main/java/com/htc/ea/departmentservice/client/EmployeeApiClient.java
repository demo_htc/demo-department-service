package com.htc.ea.departmentservice.client;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.htc.ea.departmentservice.model.Employee;

public interface EmployeeApiClient {
	/**
	 * consulta al microservicio employee
	 * retorna lista de empleados que esten bajo el departamento enviado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id departamento
	 * @return response entity con headers y lista de employee en el body
	 */
	ResponseEntity<List<Employee>> findEmployeesByIdDepartment(Long id);
	
}
