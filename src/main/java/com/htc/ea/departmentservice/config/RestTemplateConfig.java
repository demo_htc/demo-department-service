package com.htc.ea.departmentservice.config;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateConfig {
	
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
