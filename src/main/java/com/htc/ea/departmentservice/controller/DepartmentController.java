package com.htc.ea.departmentservice.controller;


import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.departmentservice.dto.DepartmentRequest;
import com.htc.ea.departmentservice.model.Department;
import com.htc.ea.departmentservice.service.DepartmentService;

@RestController
public class DepartmentController {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Value("${server.port}")
	private int serverPort;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private Mapper mapper;
	
	/**
	 * peticion tipo post de guardado de departamento
	 * llama a department service para realizar operacion de guardado
	 * @param request dto de entrada que se mapea a Deparment
	 * @return response entity con cabeceras de mensajes, department en el body, y http status
	 */
	@PostMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Department> save(@Valid @RequestBody DepartmentRequest request) {
		showIpAndPort("POST / "+request.toString());
		return departmentService.save(mapper.map(request, Department.class));//mapeo la entrada DeparmentRequest y obtengo sus valores y los paso al objeto Department
	}
	
	/**
	 * peticion get que obtiene un departamento en base al id
	 * @param id departamento
	 * @return response entity con cabeceras de mensajes, y department en el body si se encontro
	 */
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Department> findById(@PathVariable("id") Long id) {
		showIpAndPort("GET /"+String.valueOf(id));
		return departmentService.findById(id);
	}
	
	/**
	 * peticion get que obtiene todos los departamentos
	 * @return response entity con cabeceras de mensajes, y department en el body
	 */
	@GetMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Department>> findAll() {
		showIpAndPort("GET /");
		return departmentService.findAll();
	}
	
	/**
	 * peticion get que obtiene todos los departamentos que esten bajo el id de la organizacion
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, y lista de department en el body
	 */
	@GetMapping(value="/organizations/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Department>> findByOrganization(@PathVariable("id") Long id) {
		showIpAndPort("GET /organizations/"+String.valueOf(id));
		return departmentService.findAllByIdOrganization(id);
	}
	
	/**
	 * peticion get que obtiene todos los departamentos que esten bajo un id de organizacion
	 * ademas obtiene los empleados que esten en cada departamento
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, y lista de department con employees en el body
	 */
	@GetMapping(value="/organizations/{id}/employees",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Department>> findAllByIdOrganizationWithEmployees(@PathVariable("id") Long id) {
		showIpAndPort("GET /organizations/"+String.valueOf(id)+"/employees");
		return departmentService.findAllByIdOrganizationWithEmployees(id);
	}
	
	/**
	 * muestra en logs informacion de la peticion
	 * @param inf informacion del request de la peticion
	 */
	private void showIpAndPort(String inf) {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("IP:PORT ");
			builder.append(InetAddress.getLocalHost().getHostAddress());
			builder.append(":");
			builder.append(serverPort);
			builder.append(" REQUEST ");
			builder.append(inf);
			log.info(builder.toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
}
